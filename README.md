[toc]

# Nodejs 包教不包会学习整理

* Node.js 包教不包会原地址: https://github.com/alsotang/node-lessons

## 测试部分 （lesson6 ~ 8)

### lesson6

* 使用 mocha should istanbul执行一个服务器端测试,
通过配置package.json中的**scripts**做以下脚本：

    * server test: mocha测试执行脚本
    * server test cover: mocha测试带代码覆盖率执行脚本，会生成coverage代码覆盖率报告文件夹

### lesson7

* 使用mocha搭建一个前端脚本单元测试，使用**phanatomjs**搭建一个虚拟的浏览器测试环境
* 建立vendor文件夹，通过命令mocha init搭建一个测试原型，其中 index.html 是单元测试的入口，tests.js 是我们的测试用例文件
* 通过配置package.json中的**scripts**做以下脚本：

    * web test create [vendor/]: 浏览器端测试，[]内目录自行定义，再执行脚本初始化测试原型
    * web test phantomjs: 模拟浏览器测试，测试类似后台测试，结果输出在控制台中

### lesson8

* 先来了解一下supertest，supertest，是专门用来配合 express （准确来说是所有兼容 connect 的 web 框架）进行集成测试的
* 编写测试app.test.js，引入supertest
* 通过配置package.json中的**scripts**做以下脚本：

    * server test: mocha测试执行脚本
    * server test cover: mocha测试带代码覆盖率执行脚本，会生成coverage代码覆盖率报告文件夹

### 需要安装的插件

    npm install mocha -save-dev
    npm install should -save-dev

    # 覆盖率
    npm install istanbul -save-dev

    # 虚拟浏览器环境
    npm install mocha-phantomjs -save-dev

    # 全栈断言库
    npm install chai -save-dev

    # 集成测试抓取页面插件
    npm install supertest -save-dev

## 知识点

* 学习使用测试框架 mocha : http://mochajs.org/
* 学习使用断言库 should : https://github.com/tj/should.js
* 了解全栈的断言库 chai: http://chaijs.com/
* 学习使用测试率覆盖工具 istanbul : https://github.com/gotwarlost/istanbul
* 了解 headless 浏览器 phantomjs: http://phantomjs.org/
* 学习 supertest 的使用 (https://github.com/tj/supertest )
* supertest和superagent API一样一样的：http://visionmedia.github.io/superagent/
* 简单 Makefile 的编写 : http://blog.csdn.net/haoel/article/details/2886

> 对于Makefile的使用，当前使用npm的script，可以了解Makefile
